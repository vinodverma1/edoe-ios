//
//  FeedVc.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class FeedVc: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
 
   

    @IBOutlet weak var barbutton: UIBarButtonItem!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Feed"
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     
    }
    

    @IBOutlet weak var collectionView: UICollectionView!
    var currentIndexPath: IndexPath = IndexPath(item: 0, section: 0)
    var itemListArray = NSMutableArray()
    var activeMenuTab = 0
    
    @IBOutlet weak var publicbotamline: UILabel!
    @IBOutlet weak var friendsbottamline: UILabel!
    @IBOutlet weak var mebottamline: UILabel!
    @IBOutlet weak var Me: UIButton!
    @IBOutlet weak var Friends: UIButton!
    @IBOutlet weak var Public: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
    var actionbtnArray = [UIButton]()
     var actionsLblArray = [UILabel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.collectionViewLayout = MenuListFlowLayout()
        self.collectionView?.isPagingEnabled = true
        actionbtnArray = [Me, Friends, Public]
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 51/255 , green: 84/255 , blue: 131/255 , alpha: 1.0)
    self.navigationController?.navigationBar.titleTextAttributes  = [NSAttributedStringKey.foregroundColor:UIColor.white]
        actionsLblArray = [mebottamline,friendsbottamline,publicbotamline]
        
       
        
       
    }
    
    
    override func viewWillLayoutSubviews() {
        if let layout = self.collectionView?.collectionViewLayout as? MenuListFlowLayout {
            layout.itemSize = (self.collectionView?.frame.size)!
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var reusableIdentifier = "melistcell"
        if(indexPath.row == 1){
            reusableIdentifier = "Friendscollectioncell"
        }else if(indexPath.row == 2){
            reusableIdentifier = "Publickcollectioncell"
        }
        
        let cell: Feedbasecollectioncell = (collectionView.dequeueReusableCell(withReuseIdentifier: reusableIdentifier, for: indexPath) as? Feedbasecollectioncell)!
        
        
        //  cell.question = question
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        
        changeMenuActionColor(index:indexPath.row)
        
         changeMenuBottomLine(index:indexPath.row)
    }
    
    
    
    func moveToMenu(index: Int) {
        print("Index After = \(index)")
        let nextIndexPath = IndexPath(row: index, section: currentIndexPath.section)
        self.collectionView?.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
        currentIndexPath = nextIndexPath
        changeMenuActionColor(index:index)
        changeMenuBottomLine(index:index)
    }
    
    @IBAction func menTapped(_ sender: Any) {
        moveToMenu(index: 0)
    }
    
    @IBAction func friendsTapped(_ sender: Any) {
        moveToMenu(index: 1)
    }
    
    @IBAction func publicTapped(_ sender: Any) {
        moveToMenu(index: 2)
    }
    
  
    
    private func changeMenuActionColor (index : Int) {
        for i in 0..<actionbtnArray.count{
            if(i == index){
                actionbtnArray[i].setTitleColor(UIColor.black, for: .normal)
                //  actionbtnArray[i].titleLabel?.textColor = UIColor.black
            }else{
                actionbtnArray[i].setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    private func changeMenuBottomLine (index : Int) {
        for i in 0..<actionsLblArray.count{
            if(i == index){
                actionsLblArray[i].isHidden = false
                //  actionbtnArray[i].titleLabel?.textColor = UIColor.black
            }else{
                actionsLblArray[i].isHidden = true
            }
        }
    }

}


