//
//  PayVc.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class PayVc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      // self.tabBarController?.navigationItem.hidesBackButton = true
        //self.tabBarController?.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
      //  self.navigationController?.navigationBar.barTintColor = UIColor(red: 51/255 , green: 84/255 , blue: 131/255 , alpha: 1.0)
     self.navigationController?.navigationBar.titleTextAttributes  = [NSAttributedStringKey.foregroundColor:UIColor.white]
     //setupNavBar()
        
          self.navigationController?.navigationItem.rightBarButtonItem  = button1
    }
    

    func setupNavBar() {
        // ... more code
        navigationController?.navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "Forward") , style: .plain, target: self, action: nil)
        // ... more code
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Pay"
  //self.navigationController.navigationItem.rightBarButtonItem  = button1
        //self.navigationController?.navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "BackButton") , style: .plain, target: self, action: nil)
        // Hide the navigation bar on the this view controller
        // self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.topItem?.title = "Pay"
        // Show the navigation bar on other view controllers
        // self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    let button1 = UIBarButtonItem(image: UIImage(named: "Forward"), style: .plain, target: self, action: Selector("clickButton")) // action:#selector(Class.MethodName) for swift 3
    
    func clickButton(){
        print("button click")
    }

    @IBAction func backbttn(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
}
