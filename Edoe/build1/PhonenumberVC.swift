//
//  PhonenumberVC.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class PhonenumberVC: masterVc {
    @IBOutlet weak var okaybttn: UIButton!
    @IBOutlet weak var mobilenumber: UITextField!
    
    
    var runningNumber = ""
    var leftValue = ""
    var rightValue = ""
    var result = ""
    override func viewDidLoad() {
        super.viewDidLoad()
     self.hidekeyboard()
        
       
        mobilenumber.isEnabled = false
        mobilenumber.isUserInteractionEnabled = false
        
        mobilenumber.delegate = self
       // mobilenumber.becomeFirstResponder()
        
    
       // mobilenumber.isEnabled = false
       // mobilenumber.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if mobilenumber.text?.characters.count == 5 && string.characters.count != 0 {
//            mobilenumber.text = mobilenumber.text! + " "
//        }
//        else if mobilenumber.text?.characters.count == 7 && string.characters.count != 0 {
//            mobilenumber.text = mobilenumber.text! + "-" //String(mobilenumber.text!.characters.dropLast())
//        }
//        else if mobilenumber.text?.characters.count == 9 && string.characters.count != 0 {
//            mobilenumber.text = mobilenumber.text! + "-"
//        }
//        else if textField.text?.characters.count == 11 && string.characters.count == 0{
//            textField.text = String(textField.text!.characters.dropLast())
//        }
//        else if textField.text?.characters.count == 14 && string.characters.count != 0 {
//            textField.text = textField.text! + "-"
//        }
//        else if mobilenumber.text?.characters.count == 12 && string.characters.count == 0 {
//            mobilenumber.text = String(mobilenumber.text!.characters.dropLast())
//        }
//        if mobilenumber.text?.characters.count == 12 && string.characters.count != 0 {
//            return false
//        }
//        return true
//    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backbttn(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    
    //|| (mobilenumber.text?.characters.count)! > 11
    @IBAction func okaybttn(_ sender: Any) {
        if (mobilenumber.text?.isEmpty)!   {
            let alert = UIAlertController(title: "Alert", message: "Please enter your Mobile No.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        else if (mobilenumber.text?.characters.count)! < 12 {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter your full Mobile No.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "VerifyPhonNumberVC") as! VerifyPhonNumberVC
        VC.stringPassed = mobilenumber.text!
        
        self.navigationController?.pushViewController(VC , animated: true)
    }
    
   
    
    @IBAction func numberpressed(_ sender: UIButton) {
        
//        if runningNumber.count <= 10 {
//            runningNumber += "\(sender.tag)"
//            mobilenumber.text = runningNumber
        
       if runningNumber.count <= 11 {
            
            
            if runningNumber.count == 11 {
            
             okaybttn.backgroundColor = UIColor( red: 22/255,  green: 53/255, blue: 97/255, alpha: 1.0 )
        }
         else  {
            
            okaybttn.backgroundColor = .lightGray
        }
        }
//
        if runningNumber.count <= 11 {
            
            
            if runningNumber.count == 5 {
                
                runningNumber += " \(sender.tag)"
                mobilenumber.text = runningNumber
            }
                
                
                
            else if runningNumber.count <= 11 {
                
                
                self.runningNumber += "\(sender.tag)"
                self.mobilenumber.text = self.runningNumber
            }
        }
        
    }
    
    
    
    
   
    
    @IBAction func clearbutton(_ sender: UIButton) {
       // runningNumber = ""
        leftValue = ""
        rightValue = ""
        result = ""
        // currentOperation = .NULL
        // mobilenumber.text = ""
//         runningNumber = String(runningNumber.dropLast())
//        mobilenumber.text = runningNumber
        
        if runningNumber.count == 6 {
            
            runningNumber = String(runningNumber.dropLast(2))
            mobilenumber.text = runningNumber
            
        } else {
            
            runningNumber = String(runningNumber.dropLast())
            mobilenumber.text = runningNumber
        }
        
        
        if runningNumber.count <= 11 {
            
            
            if runningNumber.count == 12 {
                
                okaybttn.backgroundColor = UIColor( red: 22/255,  green: 53/255, blue: 97/255, alpha: 1.0 )
            }
            else  {
                
                okaybttn.backgroundColor = .lightGray
            }
    }
       
    }
    
}
