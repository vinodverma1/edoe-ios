//
//  VerifyPhonNumberVC.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class VerifyPhonNumberVC: masterVc {
    @IBOutlet weak var pincodetext: UITextField!
    @IBOutlet weak var mobilenumberlaber: UILabel!
    @IBOutlet weak var verifybttn: UIButton!
    var stringPassed = ""
    
    
    
    
    var runningNumber = ""
    var leftValue = ""
    var rightValue = ""
    var result = ""
    override func viewDidLoad() {
        super.viewDidLoad()
 self.hidekeyboard()
        
        pincodetext.delegate = self
        pincodetext.becomeFirstResponder()
        // Do any additional setup after loading the view.
        
        mobilenumberlaber.text = stringPassed
        
        pincodetext.isEnabled = false
        pincodetext.isUserInteractionEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = pincodetext.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 5
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var pincodetxtfiled: UITextField!
    @IBAction func backbttn(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func verifybttn(_ sender: Any) {
        
        if (pincodetext.text?.isEmpty)! {
            let alert = UIAlertController(title: "Alert", message: "Please enter your Code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if (pincodetext.text?.count)! < 5 {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter Five digit code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(VC , animated: true)
    }
    
    @IBAction func numberpressed(_ sender: UIButton) {
        
        if runningNumber.count <= 4 {
            runningNumber += "\(sender.tag)"
            pincodetext.text = runningNumber
            
            
            //init(repeating repeatedValue: String, count: Int)
            
           // let spaces = String(repeating: " ", count: 5)
            
          
        }
        
        
    }
    
    
    @IBAction func clearbutton(_ sender: UIButton) {
       // runningNumber = ""
       // leftValue = ""
        //rightValue = ""
       // result = ""
        // currentOperation = .NULL
        // pincodetext.text = "0"
        
         runningNumber = String(runningNumber.dropLast())
        pincodetext.text = runningNumber
       
    }
}
