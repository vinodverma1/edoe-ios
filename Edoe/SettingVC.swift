//
//  SettingVC.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var menuitem = MenuItems()
    
   

    @IBOutlet weak var Table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
Table.rowHeight = 50
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuitem.names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingTableVCCell
        let row = indexPath.row
        cell.contentlabel.text = menuitem.names[row]
        cell.show(iSpecial: menuitem.specials[row])
        cell.shows(iSpecial: menuitem.color[row])
        //, for: menuitem.prices[row])
        
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//       if  indexPath.row == 5{
//         let VC =  self.storyboard?.instantiateViewController(withIdentifier: "PayVc") as! PayVc
//         self.navigationController?.pushViewController(VC , animated: true)
    
        if  indexPath.row == 6 {
            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
            self.navigationController?.pushViewController(VC , animated: true)
        }
        
        if  indexPath.row == 10 {
            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "TermsandconditionVC") as! TermsandconditionVC
            self.navigationController?.pushViewController(VC , animated: true)
        }
        
        
        if  indexPath.row == 11{
            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            self.navigationController?.pushViewController(VC , animated: true)
        }
        
        
        
    }
    
    @IBAction func backbttn(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    
}
