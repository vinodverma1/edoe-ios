//
//  menuViewController.swift
//  memuDemo
//
//  Created by Parth Changela on 09/10/16.
//  Copyright © 2016 Parth Changela. All rights reserved.
//

import UIKit

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var qrcodeimage: UIImageView!
    @IBOutlet weak var tblTableView: UITableView!
   @IBOutlet weak var imgProfile: UIImageView!
    
    var ManuNameArray:Array = [String]()
    var iconArray:Array = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        ManuNameArray = ["Chat","Pay","Feed","Search Contacts","Scan Code","Banks & Cards","Pending" ,"Settings", "Contact Support","Customar Care","SignIn/Register","Log out"]
        iconArray = [UIImage(named:"ChatMenu")!,UIImage(named:"PayMenu")!,UIImage(named:"FeedMenu")!,UIImage(named:"SearchMenu")!,UIImage(named:"ScanMenu")!,UIImage(named:"BankMenu")!,UIImage(named:"PendingMenu")!,UIImage(named:"SettingsMenu")!,UIImage(named:"ContactMenu")!,UIImage(named:"ContactMenu")!,UIImage(named:"ContactMenu")!,UIImage(named:"ContactMenu")!]

        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
        imgProfile.layer.cornerRadius = 50

        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
        
        qrcodeimage.layer.borderWidth = 2
        qrcodeimage.layer.borderColor = UIColor.lightGray.cgColor
        qrcodeimage.layer.cornerRadius = 20
        
        qrcodeimage.layer.masksToBounds = false
        qrcodeimage.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ManuNameArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.lblMenuname.text! = ManuNameArray[indexPath.row]
        cell.imgIcon.image = iconArray[indexPath.row]
        
        return cell
    }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let revealview:SWRevealViewController = self.revealViewController()
//
       if  indexPath.row == 11 {
            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(VC , animated: true)
        
    }
//
//           // let newFrontController = UINavigationController.init(rootViewController: VC)
//
//    // revealview.pushFrontViewController(newFrontController, animated: true)
//        }
//
//        if  indexPath.row == 1 {
//            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "PayVc") as! PayVc
//            self.navigationController?.pushViewController(VC , animated: true)
//        }
//        if  indexPath.row == 2 {
//            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "FeedVc") as! FeedVc
//            self.navigationController?.pushViewController(VC , animated: true)
//        }
//        if  indexPath.row == 3 {
//            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "salectcontactVC") as! salectcontactVC
//            self.navigationController?.pushViewController(VC , animated: true)
//        }
//
//        if  indexPath.row == 4 {
//            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "MyQrCodeVc") as! MyQrCodeVc
//            self.navigationController?.pushViewController(VC , animated: true)
//        }
////        if  indexPath.row == 5{
////            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "MyQrCodeVc") as! MyQrCodeVc
////            self.navigationController?.pushViewController(VC , animated: true)
//       // }
//
//
//        if  indexPath.row == 7 {
//            let VC =  self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//            self.navigationController?.pushViewController(VC , animated: true)
//
//
//         //   let newFrontController = UINavigationController.init(rootViewController: VC)
//
//          //  revealview.pushFrontViewController(newFrontController, animated: true)
//        }
//
//        let revealviewcontroller:SWRevealViewController = self.revealViewController()
//
//        let cell:MenuCell = tableView.cellForRow(at: indexPath) as! MenuCell
//        print(cell.lblMenuname.text!)
//        if cell.lblMenuname.text! == "Home"
//        {
//            print("Home Tapped")
//            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//
//            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
//
//        }
//        if cell.lblMenuname.text! == "Message"
//        {
//            print("message Tapped")
//
//            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
//            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//
//            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
//        }
//        if cell.lblMenuname.text! == "Map"
//        {
//            print("Map Tapped")
//        }
//        if cell.lblMenuname.text! == "Setting"
//        {
//           print("setting Tapped")
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    }
}
