//
//  PaymentVC.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class PaymentVC: masterVc {

    @IBOutlet weak var splitbttn: UIButton!
    @IBOutlet weak var requestfrombttn: UIButton!
    @IBOutlet weak var sendtobttn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
     self.hidekeyboard()
        
        splitbttn.layer.shadowColor = UIColor.black.cgColor
        splitbttn.layer.shadowOpacity = 1.0
        splitbttn.layer.shadowRadius = 5
        splitbttn.layer.shadowOffset = CGSize (width: 5, height: 5)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
