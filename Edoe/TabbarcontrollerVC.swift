//
//  TabbarcontrollerVC.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class TabbarcontrollerVC: UITabBarController {
    
    var feevc = UIViewController()
    
   

    @IBOutlet weak var button: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
                    //  revealViewController().rearViewRevealWidth = 62
            button.target = revealViewController()
            button.action = "revealToggle:"
            
                   //  revealViewController().rightViewRevealWidth = 150
                    //  button.target = revealViewController()
                    //  button.action = "rightRevealToggle:"
            
        }
            if self.revealViewController() != nil {
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        }
           // self.tabBar.unselectedItemTintColor = UIColor.clear
       // self.tabBar.tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.1)
            self.tabBar.barTintColor = .white
            self.tabBar.isTranslucent = true
            
            if let items = self.tabBar.items
            {
                for item in items
                {
                    if let image = item.image
                    {
                        item.image = image.withRenderingMode( .alwaysOriginal )
                    }
                }
            }
        }
    
            
            

            
    
        
       
        // Do any additional setup after loading the view.

    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        // Hide the navigation bar on the this view controller
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        
//        // Show the navigation bar on other view controllers
//        self.navigationController?.setNavigationBarHidden(false, animated: animated)
//    }
    
    
//    var firstViewController:tabbarVC = UIViewController() as! tabbarVC
//    // The following statement is what you need
//    var customTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "YOUR_IMAGE_NAME")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "YOUR_IMAGE_NAME"))
//    firstViewController.tabBarItem = customTabBarItem

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = defaultTabBarHeight + 16.0
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newFrame
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
