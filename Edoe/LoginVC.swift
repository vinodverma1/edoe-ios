//
//  LoginVC.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class LoginVC: masterVc {
   
var nxtscreen1 = UIViewController()
    @IBOutlet weak var newsignupbttn: UIButton!
    @IBOutlet weak var loginwithfb: UIButton!
    @IBOutlet weak var forgotbttn: UIButton!
    @IBOutlet weak var loginbttn: UIButton!
    @IBOutlet weak var Passwordtxt: UITextField!
    @IBOutlet weak var phontxt: UITextField!
    @IBOutlet weak var myImageview: UIImageView!
    
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BtnlayerSetup(sender: self.loginwithfb)
        self.BtnlayerSetup(sender: self.loginbttn)
        
        loginwithfb.layer.shadowColor = UIColor.black.cgColor
        loginwithfb.layer.shadowOpacity = 1.0
        loginwithfb.layer.shadowRadius = 5
        loginwithfb.layer.shadowOffset = CGSize (width: 5, height: 5)
        loginbttn.layer.shadowColor = UIColor.black.cgColor
        loginbttn.layer.shadowOpacity = 1.0
        loginbttn.layer.shadowRadius = 5
        loginbttn.layer.shadowOffset = CGSize (width: 5, height: 5)
       Passwordtxt.delegate = self
        phontxt.delegate = self
        nxtscreen1 = storyboard?.instantiateViewController(withIdentifier: "SWRevealView") as! SWRevealViewController
        
        self.hidekeyboard()
//        Passwordtxt.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white.cgColor])
//        phontxt.attributedPlaceholder = NSAttributedString(string:"Phone Number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white.cgColor])
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = textField.text else { return true }
//        let newLength = text.count + string.count - range.length
//        return newLength <= 10
//    }

    
//
//         func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//            if phontxt.text?.count == 12 {
////
////
//            if phontxt.text?.count == 5 && string.count != 0 {
//                phontxt.text = phontxt.text! + " "
//            }
    
          
            
           // else if mobilenumber.text?.characters.count == 7 && string.characters.count != 0 {
              //  mobilenumber.text = mobilenumber.text! + "-" //String(mobilenumber.text!.characters.dropLast())
           // }
           // else if mobilenumber.text?.characters.count == 9 && string.characters.count != 0 {
            //    mobilenumber.text = mobilenumber.text! + "-"
           // }
          //  else if textField.text?.characters.count == 11 && string.characters.count == 0{
             //   textField.text = String(textField.text!.characters.dropLast())
           // }
         //   else if textField.text?.characters.count == 14 && string.characters.count != 0 {
             //   textField.text = textField.text! + "-"
           // }
//            else if phontxt.text?.characters.count == 12 && string.characters.count == 0 {
//                phontxt.text = String(phontxt.text!.characters.dropLast())
//            }
//            if phontxt.text?.characters.count == 12 && string.characters.count != 0 {
//                return false
         // }
            
          //  return true
      // }
    
    
    

//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let currentText = phontxt.text ?? ""
//        guard let stringRange = Range(range, in: currentText) else { return false }
//
//        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//
//        return updatedText.count <= 11
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
      //  self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func signup(_ sender: Any) {
        
        
        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "PhonenumberVC") as! PhonenumberVC
        self.navigationController?.pushViewController(VC , animated: true)
    }
    @IBAction func forgotbttn(_ sender: Any) {
        
        
        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "ForgotPhoneVC") as! ForgotPhoneVC
        self.navigationController?.pushViewController(VC , animated: true)
//        
//        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "forgotVC") as! forgotVC
//        self.navigationController?.pushViewController(VC , animated: true)
        
        
    }
    @IBAction func loginbttn(_ sender: Any) {
        
        let phonenumber = phontxt.text
        let password = Passwordtxt.text
        
        if (phonenumber?.isEmpty)!
         {
            let alert = UIAlertController(title: "Alert", message: "Please enter Phone number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        if (phonenumber?.count)! <= 10 {

            let alert = UIAlertController(title: "Alert", message: "Please enter your full Mobile No.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)


        }
        
        if (password?.isEmpty)! {
            let alert = UIAlertController(title: "Alert", message: "Please enter Password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.navigationController?.pushViewController(self.nxtscreen1, animated: true)
        
        
        if let myString = phontxt.text
        {
            let data = myString.data(using: .ascii, allowLossyConversion: false)
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let img = UIImage(ciImage: (filter?.outputImage)!.transformed(by: transform))
            
            
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            
            appDelegate.qrimg = img;
            //self.performSegue(withIdentifier: "Second", sender: self)
        }
        
        
        
        
        
        
    }
    @IBAction func loginfb(_ sender: Any) {
    }
    
}
