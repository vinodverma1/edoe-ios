//
//  AddpaymentVC.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class AddpaymentVC: masterVc {

    @IBOutlet weak var adddebitcardbttn: UIButton!
    @IBOutlet weak var addbnkaccbttn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BtnlayerSetup(sender: self.adddebitcardbttn)
        self.BtnlayerSetup(sender: self.addbnkaccbttn)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func backbttn(_ sender: Any) {
        
        //self.navigationController!.popViewController(animated: true)
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func addbnkacctbttn(_ sender: Any) {
        
        
        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "salectcontactVC") as! salectcontactVC
        self.navigationController?.pushViewController(VC , animated: true)
    }
    
    @IBAction func adddebitcardbttn(_ sender: Any) {
    }
}
