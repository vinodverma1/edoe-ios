//
//  Publickcollectioncell.swift
//  Edoe
//
//  Created by Pratibha on 09/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//
import UIKit
import Foundation
class Publickcollectioncell: Feedbasecollectioncell, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self;
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reusableIdentifier = "Publictablecell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath)
        
        return cell
        
    }

}
