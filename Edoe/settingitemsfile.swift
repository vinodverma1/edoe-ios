//
//  settingitemsfile.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//
import UIKit
import Foundation
class MenuItems:NSObject{
    let names:[String] = [
        "SECURITY","Touch ID & Pin",
        "Change Password","Change Phone Number",
        "PREFERENCES","Edit Profile",
        "Privacy","Blocked User",
        "Notifications","LEGAL INFORMATION",
        "User Agreement","Privacy Policy",
        "Licenses","Payment Methods Rights",
        "EDOE Cardholder Agreement","EDOE Card Privacy Policy",
        "MISCELLANEOUS","Send Feedback","Rate EDOE", "SIgn Out EDOE"]
    let prices:[Double] = [
        7.95,11.49,
        8.45,8.45,
        12.75,10.65,
        12.35,10.00,
        16.60,11.25,
        6.50,2.25,6.50,
        9.75,1.25,
        1.25,3.50,3.75
    ]
    let specials:[Bool] = [
        false,true,
        false,false,
        false,false,
        false,false,
        false,false,
        false,false,
        false,false,
        false,false,
        false,false,false,false]
    
    let color:[Bool] = [
        false,true,
        true,true,
        false,true,
        true,true,
        true,false,
        true,true,
        true,true,
        true,true,
        false,true,true,true
    
    ]
}
