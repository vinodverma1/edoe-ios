//
//  SalectBankVCCell.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class SalectBankVCCell: UITableViewCell {

    @IBOutlet weak var bankimage: UIImageView!
    @IBOutlet weak var bankname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
