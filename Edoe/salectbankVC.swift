//
//  salectbankVC.swift
//  Edoe
//
//  Created by Pratibha on 10/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class salectbankVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    var menuitem = ["Bnak of America","Bnak of America","Bnak of America","Bnak of America","Bnak of America"]
    var images = [UIImage(named:"ChatMenu"),UIImage(named:"ChatMenu"),UIImage(named:"ChatMenu"),UIImage(named:"ChatMenu"),UIImage(named:"ChatMenu")]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SalectBankVCCell
        //let row = indexPath.row
        cell.bankname.text = menuitem[indexPath.row]
      cell.bankimage.image = images[indexPath.row]
        
        
        return cell
    }
    
}
