//
//  ForgotPassword.swift
//  Edoe
//
//  Created by Pratibha on 17/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class ForgotPassword: masterVc {
    @IBOutlet weak var confirmpassword: UITextField!
    
    @IBOutlet weak var enterpassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmpassword.delegate = self
        enterpassword.delegate = self
        // self.BtnlayerSetup(sender: self.Setupfacebttn)
         self.hidekeyboard()
        // Do any additional setup after loading the view.
        
        
        enterpassword.attributedPlaceholder = NSAttributedString(string: "Enter Password",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        confirmpassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
            attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backbutton(_ sender: Any) {
        
         self.navigationController!.popViewController(animated: true)
    }
    @IBAction func Submitbutton(_ sender: Any) {
        
      
      let passwords =  enterpassword.text
       let confirmpass =  confirmpassword.text
        
        if ((passwords?.isEmpty)! || (confirmpass?.isEmpty)!) {
            
            let myAlert = UIAlertController(title:"Alert", message:"Enter your password", preferredStyle:UIAlertControllerStyle.alert)
            
            let okbutton = UIAlertAction(title:"ok", style:UIAlertActionStyle.default, handler: nil)
            
            myAlert.addAction(okbutton)
            
            self.present(myAlert, animated: true , completion: nil)
            
            return
        }
        
        if passwords != confirmpass {
            
            let myAlert = UIAlertController(title:"Alert", message:"Password do not match", preferredStyle:UIAlertControllerStyle.alert)
            
            let okbutton = UIAlertAction(title:"ok", style:UIAlertActionStyle.default, handler: nil)
            
            myAlert.addAction(okbutton)
            
            self.present(myAlert, animated: true , completion: nil)
            
            return
        }
        else {
            let myAlert = UIAlertController(title:"Alert", message:"Your Password has been changed Please Login to continue", preferredStyle:UIAlertControllerStyle.alert)
            
            let okbutton = UIAlertAction(title:"ok", style:UIAlertActionStyle.default, handler: {(action) in
                
                let VC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(VC , animated: true)
            })
            
            myAlert.addAction(okbutton)
            
            self.present(myAlert, animated: true , completion: nil)
            
            return
            
            
        }
    }
    
    
}
