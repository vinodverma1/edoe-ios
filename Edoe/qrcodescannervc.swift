//
//  ViewController.swift
//  QRReader
//
//  Created by Sebastian Hette on 17.07.2017.
//  Copyright © 2017 MAGNUMIUM. All rights reserved.
//

import UIKit
import AVFoundation

class qrcodescannervc: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var square: UIImageView!
    //var video = AVCaptureVideoPreviewLayer()
 var captureSession:AVCaptureSession?
    var qrCodeFrameView:UIView?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        inetializeCamera()
    }
        //Creating session
//        let session = AVCaptureSession()
//
//        //Define capture devcie
//        let captureDevice = AVCaptureDevice.default(for: .video)
//
//        do
//        {
//            let input = try AVCaptureDeviceInput(device: captureDevice!)
//            session.addInput(input)
//        }
//        catch
//        {
//            print ("ERROR")
//        }
//
//        let output = AVCaptureMetadataOutput()
//        session.addOutput(output)
//
//        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//
//        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
//
//        video = AVCaptureVideoPreviewLayer(session: session)
//        video.frame = view.layer.bounds
//        view.layer.addSublayer(video)
//
//        self.view.bringSubview(toFront: square)
//
//        session.startRunning()
//    }
//
//    func captureOutput(_ captureOutput: AVCaptureMetadataOutput!, didOutput metadataObjects: [AVMetadataObject]!, from connection: AVCaptureConnection!) {
//
//        if metadataObjects != nil && metadataObjects.count != 0
//        {
//            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
//            {
//                if object.type == AVMetadataObject.ObjectType.qr
//                {
//                    let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
//                    alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { (nil) in
//                        UIPasteboard.general.string = object.stringValue
//                    }))
//
//                    present(alert, animated: true, completion: nil)
//                }
//            }
//        }
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//
//}

func inetializeCamera(){
    //let captureDevice = AVCaptureDevice.default( for: .video ,withMediaType: AVMediaTypeVideo )
    
    let captureDevice =    AVCaptureDevice.default( .builtInWideAngleCamera, for: AVMediaType.video, position: .back)
    do {
        let input = try AVCaptureDeviceInput(device: captureDevice!)
        captureSession = AVCaptureSession()
        captureSession?.addInput(input)
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        captureSession?.startRunning()
        
        //view.bringSubview(toFront: messageLabel)
       // view.bringSubview(toFront: navigationBarView)
        view.bringSubview(toFront: square)
        qrCodeFrameView = UIView()
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
        }
    } catch {
        print(error)
        return
    }
}


func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
    if metadataObjects == nil || metadataObjects.count == 0 {
        qrCodeFrameView?.frame = CGRect.zero
        print("No QR code is detected")
        return
    }
    let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
    if metadataObj.type == AVMetadataObject.ObjectType.qr {
        let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
        qrCodeFrameView?.frame = barCodeObject!.bounds
        if metadataObj.stringValue != nil {
            qrCodeFrameView?.frame = CGRect.zero
            print(metadataObj.stringValue as Any)
            
        }
    }
}
}
