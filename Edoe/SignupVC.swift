//
//  SignupVC.swift
//  Edoe
//
//  Created by Pratibha on 06/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit

class SignupVC: masterVc {
    
    var uncheck:Int = 0
    var isboxclicked: Bool!
    var Boxon = UIImage(named: "fill checkbox")
    var Boxoff = UIImage(named: "checkbox")
    var check:Int = 0
    
    @IBOutlet weak var checkbox: UIButton!
   
    @IBOutlet weak var Signupbttn: UIButton!
    @IBOutlet weak var Termsconditionbttn: UIButton!
    @IBOutlet weak var Setupfacebttn: UIButton!
    @IBOutlet weak var Pintxt: UITextField!
    @IBOutlet weak var Confirmpasstxt: UITextField!
    @IBOutlet weak var Passwordtxt: UITextField!
    @IBOutlet weak var Emailtxt: UITextField!
    @IBOutlet weak var userimage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Passwordtxt.delegate = self
        Confirmpasstxt.delegate = self

        Pintxt.delegate = self
        Emailtxt.delegate = self
        self.hidekeyboard()
        self.BtnlayerSetup(sender: self.Setupfacebttn)
        //self.BtnlayerSetup(sender: self.loginbttn)
        
//        Passwordtxt.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white.cgColor])
//        Confirmpasstxt.attributedPlaceholder = NSAttributedString(string:"Phone Number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white.cgColor])
//        Pintxt.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor.white.cgColor])
//        Emailtxt.attributedPlaceholder = NSAttributedString(string:"Phone Number", attributes: [NSForegroundColorAttributeName: UIColor.white])
//
//        
//        let tapgesture = UITapGestureRecognizer( target: self, action: "tap")
//       self.view.addGestureRecognizer(tapgesture)
//    }
//    
//    func tap (){
//        
//        view.endEditing(true)
//    }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
    @IBAction func termsconditionbttn(_ sender: Any) {
//        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "TermsandconditionVC") as! TermsandconditionVC
//        self.navigationController?.pushViewController(VC , animated: true)
        
       
        
    }
    
    var pressed = false
    @IBAction func checkbox(_ sender: Any) {
        
        
        if !pressed {
            let image = UIImage(named: "fill checkbox") as UIImage!
            
          
            checkbox.setImage(image, for: UIControlState.normal)
            pressed = true
        } else {
            
            let image = UIImage(named: "checkbox") as UIImage!
          
            checkbox.setImage(image, for: UIControlState.normal)
            pressed = false
        }
        
      
    }
    @IBAction func signup(_ sender: Any) {
    }
    @IBAction func facerecogitionbttn(_ sender: Any) {
    }
    
    @IBAction func Backbttn(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func signupbttn(_ sender: Any) {
        
        let passwords =  Passwordtxt.text
        let confirmpass =  Confirmpasstxt.text
        
        
        let providedEmailAddress = Emailtxt.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if isEmailAddressValid
        {
            print("Email address is valid")
        } else {
            print("Email address is not valid")
            displayAlertMessage(messageToDisplay: "Please Enter a Valid Email address")
        }
        
    
    
   
    
  
        
        if (providedEmailAddress?.isEmpty)! {
            let alert = UIAlertController(title: "Alert", message: "Please enter your Email", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
           
      if (Passwordtxt.text?.isEmpty)!{
            let alert = UIAlertController(title: "Alert", message: "Please enter your Password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
          if (Confirmpasstxt.text?.isEmpty)! {
            let alert = UIAlertController(title: "Alert", message: "Please confirm your Password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        if passwords != confirmpass {
            
            let myAlert = UIAlertController(title:"Alert", message:"Password do not match", preferredStyle:UIAlertControllerStyle.alert)
            
            let okbutton = UIAlertAction(title:"ok", style:UIAlertActionStyle.default, handler: nil)
            
            myAlert.addAction(okbutton)
            
            self.present(myAlert, animated: true , completion: nil)
            
            return
        }
        
            
          if (Pintxt.text?.isEmpty)! {
            let alert = UIAlertController(title: "Alert", message: "Please enter your Pin", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        else  if (Pintxt.text?.count)! < 5 {
            let alert = UIAlertController(title: "Alert", message: "Please enter your 5 digit Pin", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        if (pressed == true) {
            
            print("checked already")
            
        }else  {
            
            let alert = UIAlertController(title: "Alert", message: "Please check terms and conditions", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
     if (pressed == true)
        {
            let myAlert = UIAlertController(title:"Alert", message:"You Signed up successfully Please login", preferredStyle:UIAlertControllerStyle.alert)
            
            let okbutton = UIAlertAction(title:"ok", style:UIAlertActionStyle.default, handler: {(action) in
                
                let VC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(VC , animated: true)
            })
            
            myAlert.addAction(okbutton)
            
            self.present(myAlert, animated: true , completion: nil)
            
            return
            
            
        }
            
            
            
        
    

        
//        let VC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.navigationController?.pushViewController(VC , animated: true)
    }

   func displayAlertMessage(messageToDisplay: String)
    {
    let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
    
    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        
        // Code in this block will trigger when OK button tapped.
        print("Ok button tapped");
        
    }
    
    alertController.addAction(OKAction)
    
    self.present(alertController, animated: true, completion:nil)
    
}
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
}
