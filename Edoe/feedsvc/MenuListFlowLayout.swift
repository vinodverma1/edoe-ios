//
//  QuestionsFlowLayout.swift
//  NissanVET
//
//  Created by Raurnet solutions on 09/08/17.
//  Copyright © 2017 TechMahindra. All rights reserved.
//

import UIKit

class MenuListFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        self.scrollDirection = UICollectionViewScrollDirection.horizontal
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        self.scrollDirection = UICollectionViewScrollDirection.horizontal
    }
}
