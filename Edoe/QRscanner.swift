//
//  QRscanner.swift
//  Edoe
//
//  Created by Pratibha on 13/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit
import AVFoundation
class QRscanner: UIViewController,AVCaptureMetadataOutputObjectsDelegate {


    @IBOutlet weak var square: UIImageView!
    var video = AVCaptureVideoPreviewLayer()
    let session = AVCaptureSession()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Creating session
        
        
        //Define capture devcie
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do
        {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }
        catch
        {
            print ("ERROR")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        self.view.bringSubview(toFront: square)
        
        session.startRunning()
    }
    
//    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
//
//        if metadataObjects != nil
//        {
//            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
//            {
//                if object.type == AVMetadataObject.ObjectType.qr
//                {
//                    let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
//                    alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { (nil) in
//                        UIPasteboard.general.string = object.stringValue
//                    }))
//
//                    present(alert, animated: true, completion: nil)
//                }
//            }
//        }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count != 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr {
                    if let text = object.stringValue {
                        print(text)
                        
                        session.stopRunning()
                        
                        let alertVC = UIAlertController(title: "QR Code", message: text, preferredStyle: UIAlertControllerStyle.alert)
                        alertVC.addAction(UIAlertAction(title: "Retake", style: .default, handler: {(action) in
                            self.session.startRunning()
                            
                        }))
                        
                        alertVC.addAction(UIAlertAction(title: "Copy", style: .default, handler: { (action) in
                                                    UIPasteboard.general.string = object.stringValue
                            
                            self.session.startRunning()
                                               }))
  // self.session.startRunning()
                        
                     //   alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                         //   self.session.startRunning()
                       // }))
                        present(alertVC, animated: true, completion: nil)
                    }
                } else {
                    if let text = object.stringValue {
                        print("Other code detected: ", text)
                    }
                }
            }
        }
    }
        


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
