//
//  SplitBillVC.swift
//  Edoe
//
//  Created by Pratibha on 12/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit
import Contacts

class SplitBillVC: masterVc, UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate{
        
        @IBOutlet weak var table: UITableView!
        @IBOutlet weak var searchbar: UISearchBar!
        
        var searchActive : Bool = false
        
        var filtered:[String] = []
        
        var contactStore = CNContactStore()
        var contacts = [ContactStruct]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            hidekeyboard()
            table.delegate = self
            table.dataSource = self
            
            searchbar.delegate = self
            contactStore.requestAccess(for: .contacts) { (success, error) in
                if success {
                    print("Authorization Successfull")
                }
            }
            
            fetchContacts()
            
        }
        
        //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        //    }
        //
        //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //
        //        filtered = familyName.filter({ (text) -> Bool in
        //            let tmp: NSString = text
        //            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
        //            return range.location != NSNotFound
        //        })
        //        if(filtered.count == 0){
        //            searchActive = false;
        //        } else {
        //            searchActive = true;
        //        }
        //        table.reloadData()
        //    }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if(searchActive) {
                return filtered.count
            }
            return contacts.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
            let contactToDisplay = contacts[indexPath.row]
            let contactToDisplays = contacts[indexPath.row]
            
            if(searchActive){
                cell.textLabel?.text = contactToDisplays.givenName + " " + contactToDisplay.familyName
                cell.detailTextLabel?.text = contactToDisplays.number
            } else{
                
                cell.textLabel?.text = contactToDisplay.givenName + " " + contactToDisplay.familyName
                cell.detailTextLabel?.text = contactToDisplay.number
            }
            
            
            
            
            
            return cell
        }
        
        func fetchContacts() {
            
            let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
            let request = CNContactFetchRequest(keysToFetch: key)
            try! contactStore.enumerateContacts(with: request) { (contact, stoppingPointer) in
                
                let name = contact.givenName
                let familyName = contact.familyName
                let number = contact.phoneNumbers.first?.value.stringValue
                
                let contactToAppend = ContactStruct(givenName: name, familyName: familyName, number: number!)
                
                self.contacts.append(contactToAppend)
            }
            table.reloadData()
        }
        
        
        
        
        @IBAction func backbttn(_ sender: Any) {
            
            self.navigationController!.popViewController(animated: true)
        }
        
        
        
        
}

