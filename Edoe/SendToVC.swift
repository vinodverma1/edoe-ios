//
//  SendToVC.swift
//  Edoe
//
//  Created by Pratibha on 12/04/18.
//  Copyright © 2018 pratik. All rights reserved.
//

import UIKit
import Contacts
import AVFoundation
    class SendToVC: masterVc, UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate, AVCaptureMetadataOutputObjectsDelegate{
        
        @IBOutlet weak var table: UITableView!
        @IBOutlet weak var searchbar: UISearchBar!
        @IBOutlet weak var downarrowimage: UIImageView!
       
        @IBOutlet weak var camraview: UIView!
        @IBOutlet weak var scanbttn: UIButton!
        @IBOutlet weak var image: UIImageView!
        @IBOutlet weak var QrCodeimage: UIImageView!
        @IBOutlet weak var Hideviewbttn: UIButton!
        @IBOutlet weak var qrCodeView: UIView!
        @IBOutlet weak var QrCodebttn: UIButton!
        var searchActive : Bool = false
        var video = AVCaptureVideoPreviewLayer()
        var filtered:[String] = []
        
        var contactStore = CNContactStore()
        var contacts = [ContactStruct]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            qrCodeView.isHidden = true
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            // qrImgView.image = appDelegate.qrImg
            QrCodeimage.image = appDelegate.qrimg
           
            table.delegate = self
            table.dataSource = self
            hidekeyboard()
            searchbar.delegate = self
            contactStore.requestAccess(for: .contacts) { (success, error) in
                if success {
                    print("Authorization Successfull")
                }
            }
            
            fetchContacts()
            
        }
        
        //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        //    }
        //
        //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        //    }
        //
        //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //
        //        filtered = familyName.filter({ (text) -> Bool in
        //            let tmp: NSString = text
        //            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
        //            return range.location != NSNotFound
        //        })
        //        if(filtered.count == 0){
        //            searchActive = false;
        //        } else {
        //            searchActive = true;
        //        }
        //        table.reloadData()
        //    }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if(searchActive) {
                return filtered.count
            }
            return contacts.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
            let contactToDisplay = contacts[indexPath.row]
            let contactToDisplays = contacts[indexPath.row]
            
            if(searchActive){
                cell.textLabel?.text = contactToDisplays.givenName + " " + contactToDisplay.familyName
                cell.detailTextLabel?.text = contactToDisplays.number
            } else{
                
                cell.textLabel?.text = contactToDisplay.givenName + " " + contactToDisplay.familyName
                cell.detailTextLabel?.text = contactToDisplay.number
            }
            
            
            
            
            
            return cell
        }
        
        func fetchContacts() {
            
            let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
            let request = CNContactFetchRequest(keysToFetch: key)
            try! contactStore.enumerateContacts(with: request) { (contact, stoppingPointer) in
                
                let name = contact.givenName
                let familyName = contact.familyName
                let number = contact.phoneNumbers.first?.value.stringValue
                
                let contactToAppend = ContactStruct(givenName: name, familyName: familyName, number: number!)
                
                self.contacts.append(contactToAppend)
            }
            table.reloadData()
        }
        
        
      func touchesBegan(_ touches: Set<AnyHashable>, with event: UIEvent) {
        var touch: UITouch? = touches.first as? UITouch
            //location is relative to the current view
            // do something with the touched point
        if touch?.view != view {
                qrCodeView.isHidden = true
            }
        }

        
        func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
            
            if metadataObjects != nil && metadataObjects.count != 0
            {
                if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
                {
                    if object.type == AVMetadataObject.ObjectType.qr
                    {
                        let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
                        alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { (nil) in
                            UIPasteboard.general.string = object.stringValue
                        }))
                        
                        present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        
        
        
        @IBAction func backbttn(_ sender: Any) {
            
            self.navigationController!.popViewController(animated: true)
        }
        
        
        @IBAction func showviewbttnaction(_ sender: Any) {
            qrCodeView.isHidden = false
            downarrowimage.isHidden = true
            
        }
        
        @IBAction func HideView(_ sender: Any) {
            qrCodeView.isHidden = true
            downarrowimage.isHidden = false
        }
        
        @IBAction func scanbttnaction(_ sender: Any) {
            
        }
}


