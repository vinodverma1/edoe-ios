//
//  ContactStruct.swift
//  AccessContacts
//
//  Created by Yash Patel on 02/12/17.
//  Copyright © 2017 Yash Patel. All rights reserved.
//

import Foundation

struct ContactStruct {
    var givenName: String
    var familyName: String
    var number: String
}
//import Foundation
//import ContactsUI
//
//class PhoneContacts {
//
//    class func getContacts(filter: ContactsFilter = .none) -> [CNContact] { //  ContactsFilter is Enum find it below
//
//        let contactStore = CNContactStore()
//        let keysToFetch = [
//            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
//            CNContactPhoneNumbersKey,
//            CNContactEmailAddressesKey,
//            CNContactThumbnailImageDataKey] as [Any]
//
//        var allContainers: [CNContainer] = []
//        do {
//            allContainers = try contactStore.containers(matching: nil)
//        } catch {
//            Debug.Log(message: "Error fetching containers") // you can use print()
//        }
//
//        var results: [CNContact] = []
//
//        for container in allContainers {
//            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
//
//            do {
//                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
//                results.append(contentsOf: containerResults)
//            } catch {
//                Debug.Log(message: "Error fetching containers")
//            }
//        }
//        return results
//    }

